jQuery(document).ready(function ($) {
	$('.burger-menu').click(function () {
		$(this).toggleClass('close');
	});
	$('.articles .item').each(function () {
		if ($(this).find('img').length == 0) {
			$(this).addClass('no-img');
		}
	});
	$(window).load(function () {
		var $grid = $('.articles .items').masonry({
			itemSelector: '.item',
			gutter: 30,
			columnWidth: '.sizer',
		});
	});
	if($('.blog .banner').length){
		$('.blog').css('padding-top','0');
	}
});
	var map;
	function initMap() {
		var markerMap = {lat: 55.755732,lng: 37.617033};
		map = new google.maps.Map(document.getElementById('map'), {
			center: {
				lat: 55.755732,
				lng: 37.617033,
			},

			zoom: 18
		});
		var marker = new google.maps.Marker({position: markerMap, map: map});
	}
