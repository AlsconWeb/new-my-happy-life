<?php 
/**
 * Author: Alex Lavihyn
 * Author URI: https://www.upwork.com/freelancers/~015d44c720e5f4462a
 * Template Name: Home page
 */

get_header();

$id = $post->ID;
// banner
$logo = get_field('logo_color', 'options');
$banner = get_field('main_banner', 'options');

// Blocks 

$b1_title = get_field('b1_title', $id);
$b1_sub_title = get_field('b1_sub_title', $id);
$b1_text = get_field('b1_text', $id);
$b1_img = get_field('b1_img', $id);

$b2_title = get_field('b2_title', $id);
$b2_select_post_count = get_field('b2_select_post_count', $id);
$b2_select_cat_post = get_field('b2_select_cat_post', $id);

$b3_title = get_field('b3_title', $id);
$b3_text = get_field('b3_text', $id);
$b3_img = get_field('b3_img', $id);

$b4_title = get_field('b4_title', $id);
$b4_sub_title = get_field('b4_sub_title', $id);
$b4_code_form = get_field('b4_code_form', $id);

$b5_title = get_field('b5_title', $id);
$b5_select_post_count = get_field('b5_select_post_count', $id);

$b6_title = get_field('b6_title', $id);
$b6_text = get_field('b6_text', $id);
$b6_img = get_field('b6_img', $id);
$b6_btn_text = get_field('b6_btn_text', $id);
$b6_btn_link = get_field('b6_btn_link', $id);

?>
<section>
    <div class="banner" style="background-image:url(<?php echo $banner['image'];?>">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="desc">
                        <div class="logo"><img src="<?php echo $logo;?>" alt="Logo"></div>
                        <h1><?php echo $banner['title'];?></h1>
                        <p><?php echo $banner['sub_title'];?></p>
                        <h4><?php echo $banner['sub__title'];?></h4><a class="button"
                            href="<?php echo $banner['btn_link'];?>"><?php echo $banner['btn_tetx'];?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php if($b1_title || $b1_sub_title || $b1_text ||$b1_img):?>
    <div class="description">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h2><?php echo $b1_title;?></h2>
                    <h4><?php echo $b1_sub_title;?></h4>
                    <div class="desc">
                        <div class="left-block">
                            <?php echo $b1_text; ?>
                        </div>
                        <div class="right-block"><img src="<?php echo $b1_img["url"]; ?>" alt="<?php echo $b1_img["title"]; ?>"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php endif?>

    <div class="articles">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h2><?php echo $b1_title;?></h2>
                    <div class="items">
                        <div class="sizer"></div>
                        <?php 
                            $id_cat = implode(',', $b2_select_cat_post);
                            $args = [
                                'cat' => $b2_select_cat_post, 
                                'posts_per_page' => $b2_select_post_count['value']
                            ]; 
                            $query = new WP_Query( $args ); 
                        ?>
                        <?php if ( $query->have_posts() ) : ?>
                        <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                        <div class="item">
                            <?php the_post_thumbnail();?>
                            <h5><?php the_title( );?></h5>
                            <p><?php the_excerpt();?></p>
                            <a class="link" href="<?php the_permalink();?>"></a>
                        </div>
                        <?php 
                            endwhile;
                            wp_reset_postdata();
                        ?>
                        <?php 
                           endif;
                        ?>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php if($b3_title || $b3_text || $b3_img):?>
    <div class="description">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h2><?php echo $b3_title;?></h2>
                    <div class="desc">
                        <div class="left-block">
                            <?php echo $b3_text?>
                        </div>

                        <div class="right-block">
                            <img src="<?php echo $b3_img["url"];?>" alt="<?php echo $b3_img["title"];?>">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php endif;?>

    <?php if($b4_title || $b4_sub_title || $b4_sub_title):?>
    <div class="form">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h2><?php echo $b4_title;?></h2>
                    <p><?php echo $b4_sub_title;?></p>
                    <form>
                        <div class="input">
                            <input type="text" placeholder="Ваше имя">
                        </div>
                        <div class="input">
                            <input type="email" placeholder="Ваш Email">
                        </div>
                        <div class="input">
                            <input type="submit" value="Призыв к действию">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <?php endif;?>

    <div class="cat-sections">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h2><?php echo $b1_title;?></h2>
                    <?php ?>
                    <?
                        $args = array(
                            'taxonomy'     => 'category',
                            'type'         => 'post',
                            'child_of'     => 0,
                            'parent'       => '',
                            'orderby'      => 'name',
                            'order'        => 'ASC',
                            'hide_empty'   => 1,
                            'hierarchical' => 1,
                            'exclude'      => '',
                            'include'      => '',
                            'number'       => 0,
                            'pad_counts'   => false,
                           
                        );
                        
                        $cats_btn = get_categories($args);
                       
                    ?>

                    <?php  
                        $args = [
                                'cat' => $cats_btn[0]->term_id, 
                                'posts_per_page' => $b5_select_post_count['value']
                            ]; 
                        $query = new WP_Query( $args ); 
                    ?>
                    <ul class="articles-button">
                        <?php foreach($cats_btn as $cat_btn):?>
                        <li><a href="#" data-count-posts="<?php echo $b5_select_post_count['value'];?>" data-id-cat="<?php echo $cat_btn->term_id;?>"
                                class="cat_btn"><?php echo $cat_btn->name;?></a></li>
                        <?php endforeach;?>
                    </ul>
                    <div class="items">
                        <div class="sizer"></div>
                        <?php if ( $query->have_posts() ) : ?>
                        <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                        <div class="item">
                            <?php the_post_thumbnail();?>
                            <h5><?php the_title();?></h5>
                            <p><?php the_excerpt(  );?></p>
                            <a href="<?php the_permalink();?>" class="link">
                            </a>
                        </div>
                        <?php 
                            endwhile;
                            wp_reset_postdata();
                        ?>
                        <?php 
                           endif;
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="description">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h2><?php echo $b6_title;?></h2>
                    <div class="desc">
                        <div class="left-block">
                            <?php echo $b6_text;?>
                            <a class="button" href="<?php echo $b6_btn_link?>"><?php echo $b6_btn_text?></a>
                        </div>
                        <div class="right-block">
                            <img src="<?php echo $b6_img["url"];?>" alt="<?php echo $b6_img["title"];?>">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</section>
<?php get_footer();?>