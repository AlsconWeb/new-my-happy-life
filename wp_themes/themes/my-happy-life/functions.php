<?php
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page();
	
}
register_sidebar( array(
		'name' => 'sidebare_menu',
		'id'   => 'sidebare_menu',
		'before_widget' => ' ',
		'after_widget'=>' ',
		'class'=>'cat',
		'before_title' => '<h3>',
		'after_title'   => "</h3>",

    ));
    
add_action( 'wp_enqueue_scripts', 'theme_add_scripts' );
function theme_add_scripts() {
    //add scripts
	wp_enqueue_script( 'main-js', get_template_directory_uri() .'/js/build.js', array('jquery'), '1.0', true );
	wp_enqueue_script( 'ajax_js', get_template_directory_uri(  ). '/js/ajaxPost.js', array('main-js'), '1.0', true );
	wp_enqueue_script( 'Gmap', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAiSqq1pexHGvlSEj3vv8Uu957SZGcIYZg&amp;callback=initMap', array('ajax_js'), '1.0', true );
	wp_localize_script( 'ajax_js', 'ajax_url', array( 
		'url' => admin_url('admin-ajax.php'),
	) );

    //add style
    wp_enqueue_style( 'main-style', get_template_directory_uri() .'/css/main.css');
}

add_action( 'after_setup_theme', 'theme_register_nav_menu' );
function theme_register_nav_menu() {
	register_nav_menu( 'main', 'Main Menu' );
}

if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'post-thumbnails' );
		set_post_thumbnail_size( 340, 289, true );
}

add_filter('excerpt_more', function($more) {
	return '...';
});

add_action('wp_ajax_cat_postAjax', 'cat_loadedAjax');
add_action('wp_ajax_nopriv_cat_postAjax', 'cat_loadedAjax');

function cat_loadedAjax(){
	$count_posts = $_POST['count_posts'];
	$cat_id = $_POST['cat_id'];

	$args = [
		'cat' => $cat_id, 
		'posts_per_page' => $count_posts
	]; 
	$query = new WP_Query( $args );
	 

	if ( $query->have_posts() ){
		// $res  .='<div class="items">';
		while ( $query->have_posts() ){ 
			$query->the_post();
			$res  .= '<div class="item">';
			$res  .= get_the_post_thumbnail();
			$res  .= '<h5>'.get_the_title().'</h5>';
			$res  .= '<p>'.get_the_excerpt().'</p>';
			$res  .= '<a href="'.get_permalink().'"  class="link">';
			$res  .='</a>';
			$res  .= '</div>';
		}
		// $res  .= '</div>';
	}
	
	die(json_encode(array('count-posts' => $count_posts, 'id'=>$cat_id, 'body'=>$res)));
}