<?php 
/**
 * Author: Alex Lavihyn
 * Author URI: https://www.upwork.com/freelancers/~015d44c720e5f4462a
 * Template Name: Contact page
 */

 $id = $post->ID;

 $title = get_field('title', $id);
 $phones = get_field('phones', $id);
 $title_email_adress = get_field('title_email_adress', $id);
 $address = get_field('adress', $id);
 

    get_header(  );
?>
<section>
    <div class="contacts">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="left-block">
                        <p><?php echo $title;?></p>
                        <ul class="tel icon-tel">
                            <?php if($phones['tel_1']): 
                                ?>
                            <li><a href="tel:<?php echo $phones['tel_1'];?>"><?php echo $phones['tel_1'];?></a></li>
                            <?php endif;
                            if($phones['tel_2']):
                            ?>
                            <li><a href="tel:<?php echo $phones['tel_2'];?>"><?php echo $phones['tel_2'];?></a></li>
                            <?php endif;?>
                        </ul>
                        <p><?php echo $title_email_adress;?></p>
                        <ul class="address">
                            <li><a class="icon-mail" href="mailto:<?php echo $address['email'];?>"><?php echo $address['email'];?></a></li>
                            <li><a class="icon-marker" href="<?php echo $address['adress_link'];?>"><?php  echo $address['adress_text'];?></a></li>
                        </ul>
                        <ul class="soc">
                            <?php if($address['whatsapp']):?>
                            <li><a class="icon-whatsapp" href="https://wa.me/<?php echo $address['whatsapp'];?>"></a></li>
                            <?php endif; if($address['viber']):?>
                            <li><a class="icon-viber" href="viber://add?number=<?php echo $address['viber'];?>"></a></li>
                            <?php endif; if($address['telegramm']):?>
                            <li><a class="icon-telegram" href="https://telegram.me/<?php echo $address['telegramm'];?>"></a></li>
                            <?php endif?>
                        </ul>
                    </div>
                    <div class="right-block">
                        <div id="map"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php 
    get_footer( );
?>