jQuery(document).ready(function($) {

	var $grid = $('.cat-sections .items').masonry({
		itemSelector: '.item',
		gutter: 30,
		columnWidth: '.sizer',
	});

	$(".cat_btn").on('click', function(e) {
		e.preventDefault();
		console.log("ajax_url ", ajax_url.url);
		$.ajax({
			type: "POST",
			url: ajax_url.url,
			data: {
				action: "cat_postAjax",
				count_posts: $(this).data("count-posts"),
				cat_id: $(this).data("id-cat")
			},
			success: function(res) {
				var resJson = JSON.parse(res);
				if (resJson.body != null) {
					$(".cat-sections .item").remove();

					$(".cat-sections .items").append(resJson.body);

					$grid.masonry("reloadItems");
					$grid.masonry("layout");
					//$(".cat-sections .items")
					//.append(resJson.body)
					//.masonry("appended", resJson.body);
				}
			}
		});
	});
});
function newFunction() {
    return "appended";
}
