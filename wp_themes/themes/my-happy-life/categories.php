<?php
/**
 * Author: Alex Lavihyn
 * Author URI: https://www.upwork.com/freelancers/~015d44c720e5f4462a
 * Template: Categories 
 */

 get_header(  );
?>
<section>
    <div class="articles categories">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="items">
                        <div class="sizer"></div>
                        <?php if ( have_posts() ) :  while ( have_posts() ) : the_post(); ?>

                        <div class="item">
                            <?php the_post_thumbnail();?>
                            <h5><?php the_title();?></h5>
                            <?php the_content();?><a class="link" href="<?php the_permalink( );?>"></a>
                        </div>
                        <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                    <?php get_sidebar(  );?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer(  );?>