<?php 
/**
 * Template Post Type:page
 */

get_header();

// $id = $post->ID;
?>
<section>
    <div class="blog">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <?php while ( have_posts() ) : the_post(); ?>
                    <div class="banner" style="background-image:url(<?php echo get_the_post_thumbnail_url($post->ID, 'full');?>)">
                        <h1><?php the_title();?></h1>
                    </div>

                    <?php the_content();?>
                    <?php endwhile;?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer();?>