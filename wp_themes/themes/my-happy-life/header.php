<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php bloginfo( 'name' )?> | <?php bloginfo( 'description' )?></title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900" rel="stylesheet">
    <link rel="apple-touch-icon" sizes="57x57" href="<?php bloginfo( 'template_url' )?>/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php bloginfo( 'template_url' )?>/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo( 'template_url' )?>/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php bloginfo( 'template_url' )?>/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo( 'template_url' )?>/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php bloginfo( 'template_url' )?>/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php bloginfo( 'template_url' )?>/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php bloginfo( 'template_url' )?>/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php bloginfo( 'template_url' )?>/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="<?php bloginfo( 'template_url' )?>/favico/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php bloginfo( 'template_url' )?>/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php bloginfo( 'template_url' )?>/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php bloginfo( 'template_url' )?>/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php bloginfo( 'template_url' )?>/favicon//manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php bloginfo( 'template_url' )?>/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!--(if lt IE 9)-->
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <!--(endif)-->
    <?php wp_head(  );?>
</head>

<?php

$logo = get_field('logo_color', 'options');
$bg = get_field('body_bg', 'options');
?>
<?php if(is_front_page()):?>

<body <?php body_class();?> style="background-image: url(<?php echo $bg;?>);">
    <?php else:?>

    <body <?php body_class("inner-page");?> style="background-image: url(<?php echo $bg;?>);">
        <?php endif;?>
        <div class="wrapper">
            <header>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="logo"><a href="<?php bloginfo( 'url' )?>"><img src="<?php echo $logo;?>" alt="#"></a></div>
                            <div class="burger-menu"><span></span><span></span><span></span></div>
                            <?php 
                            $arg = [
                                'theme_location'  => 'main',
                                'menu'            => 'Menu', 
                                'container'       => '', 
                                'container_class' => '', 
                                'container_id'    => 'main-menu',
                                'menu_class'      => 'menu', 
                                'menu_id'         => '',
                                'echo'            => true,
                                'fallback_cb'     => 'wp_page_menu',
                                'before'          => '',
                                'after'           => '',
                                'link_before'     => '',
                                'link_after'      => '',
                                'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                                'depth'           => 0,
                                'walker'          => '',
                            ];
                            wp_nav_menu(  $arg ); 
                        ?>
                        </div>
                    </div>
                </div>
            </header>