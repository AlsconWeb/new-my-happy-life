<?php 
$logo = get_field('logo_white', 'options');
?>

<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="logo"><img src="<?php echo $logo;?>" alt="Logo">
                    <h6><?php bloginfo( 'description' );?></h6>
                </div>
                <?php 
                        
                    wp_nav_menu( 'Menu' ); 
                 ?>
                <form>
                    <div class="input">
                        <input type="text" placeholder="Ваше имя">
                    </div>
                    <div class="input">
                        <input type="email" placeholder="Ваш Email">
                    </div>
                    <div class="input">
                        <input type="submit" value="Призыв к действию">
                    </div>
                </form>
            </div>
        </div>
    </div>
</footer>
</div>
<!-- <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAiSqq1pexHGvlSEj3vv8Uu957SZGcIYZg&amp;callback=initMap"></script> -->
<?php wp_footer();?>
</body>

</html>