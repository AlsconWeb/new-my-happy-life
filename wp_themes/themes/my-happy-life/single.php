<?php 
get_header();
?>
<section>
    <div class="blog">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <?php if ( have_posts() ) :  while ( have_posts() ) : the_post(); ?>

                    <h1><?php the_title();?></h1>
                    <?php echo get_the_post_thumbnail();?>
                    <?php the_content();?>

                    <?php endwhile; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="comments">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="comments-form">
                        <?php comments_template( ); ?>
                        <div id="disqus_thread"></div>
                        <script>
                        /**
                         *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT 
                         *  THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR 
                         *  PLATFORM OR CMS.
                         *  
                         *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: 
                         *  https://disqus.com/admin/universalcode/#configuration-variables
                         */

                        var disqus_config = function() {
                            // Replace PAGE_URL with your page's canonical URL variable
                            this.page.url = window.location.href;

                            // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                            this.page.identifier = <?php echo $post->ID;?>
                        };


                        (function() { // REQUIRED CONFIGURATION VARIABLE: EDIT THE SHORTNAME BELOW
                            var d = document,
                                s = d.createElement('script');

                            // IMPORTANT: Replace EXAMPLE with your forum shortname!
                            s.src = 'http-alscon-clients-com/embed.js';

                            s.setAttribute('data-timestamp', +new Date());
                            (d.head || d.body).appendChild(s);
                        })();
                        </script>
                        <noscript>
                            Please enable JavaScript to view the
                            <a href="https://disqus.com/?ref_noscript" rel="nofollow">
                                comments powered by Disqus.
                            </a>
                        </noscript>


                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="last-posts">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <?php  
                        $cat_id = get_the_category();
                        $id = $post->ID;
                        $args = [
                            'cat' => $cat_id[0]->term_id, 
                            'post__not_in' => array($id),
                            'posts_per_page' => 3, 
                        ]; 
                        $query = new WP_Query( $args ); 
                        
                        if(count($query->posts) > 1):
                    ?>
                    <h2>ПОСЛЕДНИЕ ПОСТЫ</h2>
                    <div class="items">
                        <?php if ( $query->have_posts() ) : ?>
                        <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                        <div class="item">
                            <?php if(has_post_thumbnail()):?>
                            <?php the_post_thumbnail();?>
                            <?php else:?>
                            <img src="https://via.placeholder.com/260x290" alt="">
                            <?php endif;?>
                            <h5><?php the_title();?></h5><a class="link" href="<?php the_permalink();?>"></a>
                        </div>
                        <?php 
                            endwhile;
                            wp_reset_postdata();
                        ?>
                        <?php 
                           endif;
                        ?>
                    </div>
                    <?php endif;?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer(  );?>