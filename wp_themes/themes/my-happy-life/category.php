<?php
/**
 * Author: Alex Lavihyn
 * Author URI: https://www.upwork.com/freelancers/~015d44c720e5f4462a
 * Template: Categories 
 */

 get_header(  );
 $id = $post->ID;
// banner
$logo = get_field('logo_color', 'options');
$banner = get_field('main_banner', 'options');
?>
<section>
    <div class="banner" style="background-image:url(<?php echo $banner['image'];?>">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="desc">
                        <div class="logo"><img src="<?php echo $logo;?>" alt="Logo"></div>
                        <h1><?php echo $banner['title'];?></h1>
                        <p><?php echo $banner['sub_title'];?></p>
                        <h4><?php echo $banner['sub__title'];?></h4><a class="button"
                            href="<?php echo $banner['btn_link'];?>"><?php echo $banner['btn_tetx'];?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="articles categories">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="items">
                        <div class="sizer"></div>
                        <?php if ( have_posts() ) :  while ( have_posts() ) : the_post(); ?>

                        <div class="item">
                            <?php the_post_thumbnail();?>
                            <h5><?php the_title();?></h5>
                            <?php the_excerpt();?><a class="link" href="<?php the_permalink( );?>"></a>
                        </div>
                        <?php endwhile; ?>
                        <?php endif; ?>
                    </div>
                    <?php get_sidebar();?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer(  );?>