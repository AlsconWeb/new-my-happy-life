<?php 
/**
 * The template for displaying 404 pages (Not Found)
 */

get_header();
?>

<section>
    <div class="page404">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="banner" style="background-image:url(<?php bloginfo( 'template_url' );?>/img/image.jpg);">
                        <h1>404</h1><a class="button" href="<?php bloginfo('url')?>">На главную</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php get_footer(  );?>